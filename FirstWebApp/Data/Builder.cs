﻿using System;

namespace FirstWebApp.Data
{
    public class Builder
    {
        private static string sqliteConnectionString = @"Data Source=wwwroot/Data/data.db";

        public static IFreeSql FSql { get; } = new FreeSql.FreeSqlBuilder()
            .UseConnectionString(FreeSql.DataType.Sqlite, sqliteConnectionString)
            .UseAutoSyncStructure(true) //自动同步实体结构
            .UseMonitorCommand(cmd => Console.Write(cmd.CommandText))
            .Build();

    }
}
