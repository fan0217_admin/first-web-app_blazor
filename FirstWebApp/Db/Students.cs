﻿using System.Threading.Tasks;

namespace FirstWebApp.Db
{
    public class Students
    {

        #region CRUD 

        public static async Task<int> AddAsync(Models.Students item)
        {
            return await Data.Builder.FSql
                             .Insert(item)
                             .ExecuteAffrowsAsync();
        }

        public static async Task<int> UpdateAsync(Models.Students item)
        {
            return await Data.Builder.FSql
                             .Update<Models.Students>()
                             .SetSource(item)
                             .ExecuteAffrowsAsync();
        }

        public static async Task<int> DeleteAsync(int id)
        {
            return await Data.Builder.FSql
                             .Delete<Models.Students>()
                             .Where(w => w.StudentID == id)
                             .ExecuteAffrowsAsync();
        }

        public static async Task<int> GetMaxIdAsync()
        {
            var id = Data.Builder.FSql
                         .Select<Models.Students>()
                         .Max(m => m.StudentID);
            return await Task.FromResult(id);
        }

        public static async Task<Models.Students> GetModelAsync(int id)
        {
            return await Data.Builder.FSql
                             .Select<Models.Students>()
                             .Where(w => w.StudentID == id)
                             .ToOneAsync();
        }

        #endregion CRUD

    }
}
