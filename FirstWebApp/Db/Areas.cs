﻿using System.Threading.Tasks;

namespace FirstWebApp.Db
{
    public class Areas
    {
        #region CRUD 

        public static async Task<int> AddAsync(Models.Areas item)
        {
            return await Data.Builder.FSql
                             .Insert(item)
                             .ExecuteAffrowsAsync();
        }

        public static async Task<int> UpdateAsync(Models.Areas item)
        {
            return await Data.Builder.FSql
                             .Update<Models.Areas>()
                             .SetSource(item)
                             .ExecuteAffrowsAsync();
        }

        public static async Task<int> DeleteAsync(int id)
        {
            return await Data.Builder.FSql
                             .Delete<Models.Areas>()
                             .Where(w => w.AreaID == id)
                             .ExecuteAffrowsAsync();
        }

        public static async Task<int> GetMaxIdAsync()
        {
            var id = Data.Builder.FSql
                         .Select<Models.Areas>()
                         .Max(m => m.AreaID);
            return await Task.FromResult(id);
        }

        public static async Task<Models.Areas> GetModelAsync(int id)
        {
            return await Data.Builder.FSql
                             .Select<Models.Areas>()
                             .Where(w => w.AreaID == id)
                             .ToOneAsync();
        }

        #endregion CRUD

    }
}
