﻿var menus = [
    {
        id: "10", text: "信息中心", icon: "fa fa-windows", isOpen: true, children: [
            { id: "1011", text: "学生 ", url: "../Students", targetType: "iframe-tab", icon: "fa fa-file-text-o" },
            { id: "1012", text: "地区 ", url: "../Areas", targetType: "iframe-tab", icon: "fa fa-table" },
            { id: "1013", text: "组件通信 ", url: "../Parent", targetType: "iframe-tab", icon: "fa fa-table" }
        ]
    }
];
