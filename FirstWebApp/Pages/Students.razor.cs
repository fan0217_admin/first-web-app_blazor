﻿using AntDesign;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FirstWebApp.Pages
{
    public partial class Students
    {

        #region 声明

        [Inject] protected MessageService Message { get; set; }

        protected List<Models.Students> List = [];
        protected Models.Students student = new();
        protected string _title = "学生";
        protected int _pageSize = 15;
        protected int _pageIndex = 0;
        protected bool _loading = false;
        protected int _selectID = 0;

        #endregion 声明

        #region 页面事件

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                await GetItemsAsync();
                StateHasChanged();
            }
        }

        #endregion 页面事件

        #region 处理数据

        protected async Task GetItemsAsync()
        {
            _loading = true;

            var list = Data.Builder.FSql
                           .Select<Models.Students>()
                           .OrderBy(o => o.StudentName);

            if (!string.IsNullOrEmpty(_queryValue))
                list = list.Where(s => s.StudentName.Contains(_queryValue));

            List = await list.ToListAsync();
            _loading = false;
        }

        protected async Task OnRefreshAsync()
        {
            await GetItemsAsync();
            _ = Message.Info("刷新成功", 2);
        }

        #endregion 处理数据

        #region 数据搜索

        protected string _queryValue { get; set; }

        protected async Task OnQueryAsync()
        {
            await GetItemsAsync();
            _ = Message.Info("查询成功", 2);
        }

        protected async Task OnClearQueryAsync()
        {
            _queryValue = string.Empty;
            await GetItemsAsync();
        }

        #endregion 数据搜索

        #region 数据编辑抽屉

        protected bool _editDrawerVisible = false;

        protected async Task CloseEditDrawerAsync()
        {
            await Task.Run(() =>
            {
                _selectID = 0;
                _editDrawerVisible = false;
            });
        }

        protected async Task OnCloseEditDrawerAsync() => await CloseEditDrawerAsync();

        protected async Task OnOpenAddDrawerAsync()
        {
            _selectID = 0;
            _editDrawerVisible = true;
            student = new();
            student.StudentID = await Db.Students.GetMaxIdAsync() + 1;
        }

        protected async Task OnOpenEditDrawerAsync(int id)
        {
            _selectID = id;
            _editDrawerVisible = true;
            student = await Db.Students.GetModelAsync(id);
        }

        #endregion 数据编辑抽屉

        #region 编辑数据

        protected async Task OnSaveAsync()
        {
            if (_selectID == 0)
            {
                await Db.Students.AddAsync(student);
                await GetItemsAsync();
                _editDrawerVisible = false;
                _ = Message.Info("添加成功", 2);
            }
            else
            {
                await Db.Students.UpdateAsync(student);
                await GetItemsAsync();
                _editDrawerVisible = false;
                _ = Message.Info("更新成功", 2);
            }
        }

        protected async Task OnDeleteAsync(int id)
        {
            await Db.Students.DeleteAsync(id);
            await GetItemsAsync();
            _ = Message.Info("删除成功", 2);
        }

        #endregion 编辑数据

    }
}
