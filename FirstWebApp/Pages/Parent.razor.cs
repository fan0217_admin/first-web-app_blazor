﻿using AntDesign;
using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;

namespace FirstWebApp.Pages
{
    public partial class Parent
    {
        [Inject] protected MessageService Message { get; set; }
        protected string _msg = "";
        protected string _queryValue = "";

        protected async Task OnHandleAsync(string value)
        {
            _msg = value;
            await Message.Info($"当前时间：{value}", 2);
        }

        protected async Task OnQueryAsync(string value)
        {
            _queryValue = value;
            await Message.Info($"搜索关键字：{_queryValue}", 2);
        }

        protected async Task OnClearQueryAsync()
        {
            _queryValue = "";
            await Message.Info($"搜索关键字：{_queryValue}", 2);
        }


    }
}
