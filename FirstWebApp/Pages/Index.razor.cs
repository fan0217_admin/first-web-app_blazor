﻿using Microsoft.AspNetCore.Components;

namespace FirstWebApp.Pages
{
    public partial class Index
    {
        [Inject] protected NavigationManager Nav { get; set; }

        protected override void OnInitialized()
        {
            Nav.NavigateTo("Main", true);
        }

    }
}
