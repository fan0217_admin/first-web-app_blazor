﻿using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;

namespace FirstWebApp.Pages.Components
{
    public partial class SearchBox
    {
        [Parameter] public string SearchValue { get; set; }
        [Parameter] public string Placeholder { get; set; } = "输入查询关键字";
        [Parameter] public string Width { get; set; } = "200px";
        [Parameter] public EventCallback<string> OnQueryAsync { get; set; }
        [Parameter] public EventCallback<string> OnClearQueryAsync { get; set; }

        protected async Task SearchAsync()
        {
            if (OnQueryAsync.HasDelegate)
                await OnQueryAsync.InvokeAsync(SearchValue);
        }

        protected async Task ClearSearchAsync()
        {
            if (OnClearQueryAsync.HasDelegate)
                await OnClearQueryAsync.InvokeAsync("");
        }
    }
}
