﻿using Microsoft.AspNetCore.Components;
using System;
using System.Threading.Tasks;

namespace FirstWebApp.Pages.Components
{
    public partial class Son
    {
        [Parameter] public EventCallback<string> OnHandleAsync { get; set; }

        protected async Task HandleAsync()
        {
            if (OnHandleAsync.HasDelegate)
                await OnHandleAsync.InvokeAsync(DateTime.Now.ToString());
        }
    }
}
