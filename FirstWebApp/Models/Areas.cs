﻿using FreeSql.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace FirstWebApp.Models
{
    public class Areas
    {
        [Column(IsPrimary = true)]
        [Key]
        [Display(Name = "ID")]
        public int AreaID { get; set; }

        [Display(Name = "地区名称")]
        [Required(ErrorMessage = "请输入地区名称")]
        public string AreaName { get; set; }
    }
}
