﻿using FreeSql.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace FirstWebApp.Models
{
    public class Students
    {
        [Column(IsPrimary = true)]
        [Key]
        [Display(Name = "ID")]
        public int StudentID { get; set; }

        [Display(Name = "学生名称")]
        [Required(ErrorMessage = "请输入学生名称")]
        public string StudentName { get; set; }
    }
}
