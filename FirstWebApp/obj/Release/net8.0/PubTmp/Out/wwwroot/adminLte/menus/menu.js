﻿var menus = [
    {
        id: "10", text: "信息中心", icon: "fa fa-windows", isOpen: true, children: [
            { id: "1011", text: "学生 ", url: "../Students", targetType: "iframe-tab", icon: "fa fa-file-text-o" },
            { id: "1012", text: "地区 ", url: "../Areas", targetType: "iframe-tab", icon: "fa fa-table" },
            { id: "1013", text: "组件通信 ", url: "../Parent", targetType: "iframe-tab", icon: "fa fa-table" }
        ]
    },
    {
        id: "20", text: "基础资料", icon: "fa fa-edit", children: [
            { id: "2011", text: "客户 ", url: "../404", targetType: "iframe-tab", icon: "fa fa-edit" },
            { id: "2014", text: "职员 ", url: "../404", targetType: "iframe-tab", icon: "fa fa-edit" },
            { id: "2015", text: "地区 ", url: "../404", targetType: "iframe-tab", icon: "fa fa-edit" }
        ]
    },
    {
        id: "80", text: "权限管理", icon: "fa fa-cogs", children: [
            { id: "8011", text: "用户管理 ", url: "../404", targetType: "iframe-tab", icon: "fa fa-cogs" },
            { id: "8012", text: "角色管理 ", url: "../404", targetType: "iframe-tab", icon: "fa fa-cogs" },
            { id: "8013", text: "用户角色信息 ", url: "../404", targetType: "iframe-tab", icon: "fa fa-cogs" },
            { id: "8014", text: "角色权限配置 ", url: "../404", targetType: "iframe-tab", icon: "fa fa-cogs" }
        ]
    },
    {
        id: "90", text: "系统管理", icon: "fa fa-cog", children: [
            { id: "9011", text: "系统参数设置 ", url: "../404", targetType: "iframe-tab", icon: "fa fa-cog" },
            { id: "9012", text: "系统会计科目 ", url: "../404", targetType: "iframe-tab", icon: "fa fa-cog" },
            { id: "9013", text: "备份数据库 ", url: "../404", targetType: "iframe-tab", icon: "fa fa-cog" },
            { id: "9014", text: "收缩数据库 ", url: "../404", targetType: "iframe-tab", icon: "fa fa-cog" },
            { id: "9015", text: "联系我们 ", url: "../404", targetType: "iframe-tab", icon: "fa fa-cog" },
            { id: "9016", text: "系统日志 ", url: "../404", targetType: "iframe-tab", icon: "fa fa-cog" },
            { id: "9017", text: "关于 ", url: "../404", targetType: "iframe-tab", icon: "fa fa-cog" }
        ]
    }
];
