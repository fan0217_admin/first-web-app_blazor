﻿var menus = [
    {
        id: "10", text: "信息中心", icon: "fa fa-windows", isOpen: true, children: [
            { id: "1011", text: "学生 ", url: "/Students", targetType: "iframe-tab", icon: "fa fa-file-text-o" },
            { id: "1012", text: "信息许可(测试)  ", url: "DataCenter/Report.aspx", targetType: "iframe-tab", icon: "fa fa-table" }
        ]
    },
    {
        id: "20", text: "基础资料", icon: "fa fa-edit", children: [
            { id: "2011", text: "客户 ", url: "Finance/KeepAccounts/Report.aspx", targetType: "iframe-tab", icon: "fa fa-edit" },
            { id: "2012", text: "代理商 ", url: "BasicData/AccountingSubject.aspx", targetType: "iframe-tab", icon: "fa fa-edit" },
            { id: "2013", text: "软件 ", url: "Finance/ExchangeRate/Step1.aspx", targetType: "iframe-tab", icon: "fa fa-edit" },
            { id: "2014", text: "职员 ", url: "Finance/KeepAccounts/Report.aspx", targetType: "iframe-tab", icon: "fa fa-edit" },
            { id: "2015", text: "地区 ", url: "BasicData/AccountingSubject.aspx", targetType: "iframe-tab", icon: "fa fa-edit" }
        ]
    },
    {
        id: "25", text: "支付平台", icon: "fa fa-folder-open", children: [
            { id: "2511", text: "年费 ", url: "FixedAssets/FixedAssetsCards.aspx", targetType: "iframe-tab", icon: "fa fa-leaf" },
            { id: "2512", text: "价格计算 ", url: "FixedAssets/FixedAssetsType.aspx", targetType: "iframe-tab", icon: "fa fa-leaf" }
        ]
    },
    {
        id: "30", text: "凭证查询", icon: "fa fa-search", children: [
            { id: "3011", text: "记账凭证查询 ", url: "Finance/Report/KeepAccountsQuery.aspx", targetType: "iframe-tab", icon: "fa fa-search" },
            { id: "3012", text: "记账凭证序时簿 ", url: "Finance/Report/KeepAccountsChronological.aspx", targetType: "iframe-tab", icon: "fa fa-search" }
        ]
    },
    {
        id: "40", text: "信息交互", icon: "fa fa-table", children: [
            { id: "4011", text: "信息交互中心 ", url: "Finance/Report/KeepAccountsBalanceForeign.aspx", targetType: "iframe-tab", icon: "fa fa-table" }
        ]
    },
    {
        id: "50", text: "广告管理", icon: "fa fa-list-ul", children: [
            { id: "5011", text: "广告设置 ", url: "Finance/ClientBalance/ReceivableBalance.aspx", targetType: "iframe-tab", icon: "fa fa-list-ul" },
            { id: "5012", text: "广告投放人 ", url: "Finance/ClientBalance/PayableBalance.aspx", targetType: "iframe-tab", icon: "fa fa-list-ul" },
            { id: "5013", text: "广告类别 ", url: "Finance/ClientBalance/AdvanceReceivableBalance.aspx", targetType: "iframe-tab", icon: "fa fa-list-ul" },
            { id: "5014", text: "广告点击率 ", url: "Finance/ClientBalance/AdvancePayableBalance.aspx", targetType: "iframe-tab", icon: "fa fa-list-ul" }
        ]
    },

    {
        id: "80", text: "权限管理", icon: "fa fa-cogs", children: [
            { id: "8011", text: "用户管理 ", url: "Permission/UserInfo.aspx", targetType: "iframe-tab", icon: "fa fa-cogs" },
            { id: "8012", text: "角色管理 ", url: "Permission/RoleInfo.aspx", targetType: "iframe-tab", icon: "fa fa-cogs" },
            { id: "8013", text: "用户角色信息 ", url: "Permission/UserToRole.aspx", targetType: "iframe-tab", icon: "fa fa-cogs" },
            { id: "8014", text: "角色权限配置 ", url: "Permission/RoleToMenu.aspx", targetType: "iframe-tab", icon: "fa fa-cogs" }
        ]
    },
    {
        id: "90", text: "系统管理", icon: "fa fa-cog", children: [
            { id: "9011", text: "系统参数设置 ", url: "System/SetUserParam.aspx", targetType: "iframe-tab", icon: "fa fa-cog" },
            { id: "9012", text: "系统会计科目 ", url: "System/AccountingSubjectSys.aspx", targetType: "iframe-tab", icon: "fa fa-cog" },
            { id: "9013", text: "备份数据库 ", url: "Database/Backup.aspx", targetType: "iframe-tab", icon: "fa fa-cog" },
            { id: "9014", text: "收缩数据库 ", url: "Database/Shrink.aspx", targetType: "iframe-tab", icon: "fa fa-cog" },
            { id: "9015", text: "联系我们 ", url: "Help/Link.aspx", targetType: "iframe-tab", icon: "fa fa-cog" },
            { id: "9016", text: "系统日志 ", url: "Help/SysLog.aspx", targetType: "iframe-tab", icon: "fa fa-cog" },
            { id: "9017", text: "关于 ", url: "Help/About.aspx", targetType: "iframe-tab", icon: "fa fa-cog" }
        ]
    }
];
