#pragma checksum "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\Pages\Components\SearchBox.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4bedf7af2c95dbd0e6ba94944fae3dbc1c14820b"
// <auto-generated/>
#pragma warning disable 1591
namespace FirstWebApp.Pages.Components
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\_Imports.razor"
using FirstWebApp;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\_Imports.razor"
using FirstWebApp.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\_Imports.razor"
using AntDesign;

#line default
#line hidden
#nullable disable
    public partial class SearchBox : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __Blazor.FirstWebApp.Pages.Components.SearchBox.TypeInference.CreateAntDesign_Input_0(__builder, 0, 1, 
#nullable restore
#line 3 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\Pages\Components\SearchBox.razor"
                                Placeholder

#line default
#line hidden
#nullable disable
            , 2, Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.KeyboardEventArgs>(this, 
#nullable restore
#line 5 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\Pages\Components\SearchBox.razor"
                                 () => SearchAsync()

#line default
#line hidden
#nullable disable
            ), 3, 
#nullable restore
#line 6 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\Pages\Components\SearchBox.razor"
                                 $"width: {Width};"

#line default
#line hidden
#nullable disable
            , 4, 
#nullable restore
#line 4 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\Pages\Components\SearchBox.razor"
                                SearchValue

#line default
#line hidden
#nullable disable
            , 5, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => SearchValue = __value, SearchValue)), 6, () => SearchValue, 7, (__builder2) => {
                __builder2.OpenElement(8, "a");
                __builder2.AddAttribute(9, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 8 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\Pages\Components\SearchBox.razor"
                       () => ClearSearchAsync()

#line default
#line hidden
#nullable disable
                ));
                __builder2.OpenComponent<AntDesign.Icon>(10);
                __builder2.AddAttribute(11, "Type", "close");
                __builder2.CloseComponent();
                __builder2.CloseElement();
            }
            , 12, (__builder2) => {
                __builder2.OpenElement(13, "a");
                __builder2.AddAttribute(14, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 13 "C:\Users\Fans\source\repos\FirstWebApp\FirstWebApp\Pages\Components\SearchBox.razor"
                       () => SearchAsync()

#line default
#line hidden
#nullable disable
                ));
                __builder2.OpenComponent<AntDesign.Icon>(15);
                __builder2.AddAttribute(16, "Type", "search");
                __builder2.CloseComponent();
                __builder2.CloseElement();
            }
            );
        }
        #pragma warning restore 1998
    }
}
namespace __Blazor.FirstWebApp.Pages.Components.SearchBox
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateAntDesign_Input_0<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::Microsoft.AspNetCore.Components.EventCallback<global::Microsoft.AspNetCore.Components.Web.KeyboardEventArgs> __arg1, int __seq2, global::System.String __arg2, int __seq3, TValue __arg3, int __seq4, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg4, int __seq5, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg5, int __seq6, global::Microsoft.AspNetCore.Components.RenderFragment __arg6, int __seq7, global::Microsoft.AspNetCore.Components.RenderFragment __arg7)
        {
        __builder.OpenComponent<global::AntDesign.Input<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Placeholder", __arg0);
        __builder.AddAttribute(__seq1, "OnPressEnter", __arg1);
        __builder.AddAttribute(__seq2, "WrapperStyle", __arg2);
        __builder.AddAttribute(__seq3, "Value", __arg3);
        __builder.AddAttribute(__seq4, "ValueChanged", __arg4);
        __builder.AddAttribute(__seq5, "ValueExpression", __arg5);
        __builder.AddAttribute(__seq6, "Suffix", __arg6);
        __builder.AddAttribute(__seq7, "AddOnAfter", __arg7);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
