Blazor入门示例项目，ORM采用FreeSQL，前端组件采用Ant Design Blazor。

此项目只完成了FreeSQL和Ant Design Blazor的基础使用和学生、地区数据表的CRUD。

FreeSQL：[http://freesql.net/](http://http://freesql.net/)

Ant Design Blazor：[https://antblazor.com/zh-CN/](https://antblazor.com/zh-CN/)


界面预览：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0804/180301_f3d9bd00_1009370.png "1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0804/180314_5fd1b0cb_1009370.png "2.png")